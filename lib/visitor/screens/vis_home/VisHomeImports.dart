import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:base_flutter/visitor/screens/vis_home/widgets/VisHomeWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'VisHome.dart';
part 'VisHomeData.dart';