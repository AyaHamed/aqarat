part of 'VisHomeImports.dart';

class VisHome extends StatefulWidget {
  const VisHome({Key? key}) : super(key: key);

  @override
  _VisHomeState createState() => _VisHomeState();
}

class _VisHomeState extends State<VisHome> {
  VisHomeData visHomeData = VisHomeData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: BuildVisHomeAppBar(),
      body: Column(
        children: [
          BuildVisHomeFilter(),
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              children: List.generate(5, (index) => BuildVisHomeItem()),
            ),
          ),
        ],
      ),
    );
  }
}
