// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:base_flutter/customer/screens/add_offer_details/AddOfferDetailsImports.dart'
    as _i24;
import 'package:base_flutter/customer/screens/add_offer_photo/AddOfferPhotoImports.dart'
    as _i25;
import 'package:base_flutter/customer/screens/add_order/AddOrderImports.dart'
    as _i28;
import 'package:base_flutter/customer/screens/edit_ads/EditAdsImports.dart'
    as _i20;
import 'package:base_flutter/customer/screens/edit_profile/EditProfileImports.dart'
    as _i21;
import 'package:base_flutter/customer/screens/home/HomeImports.dart' as _i16;
import 'package:base_flutter/customer/screens/languages/LanguagesImports.dart'
    as _i18;
import 'package:base_flutter/customer/screens/my_ads/MyAdsImports.dart' as _i19;
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart'
    as _i17;
import 'package:base_flutter/customer/screens/offer_type/OfferTypeImports.dart'
    as _i22;
import 'package:base_flutter/customer/screens/order_type/OrderTypeImports.dart'
    as _i27;
import 'package:base_flutter/customer/screens/register/RegisterImports.dart'
    as _i29;
import 'package:base_flutter/customer/screens/success/SuccessImports.dart'
    as _i26;
import 'package:base_flutter/customer/screens/swear/SwearImports.dart' as _i23;
import 'package:base_flutter/general/screens/about/AboutImports.dart' as _i10;
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart'
    as _i6;
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart'
    as _i14;
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i13;
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart'
    as _i11;
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i5;
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart' as _i15;
import 'package:base_flutter/general/screens/login/LoginImports.dart' as _i4;
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart'
    as _i7;
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart'
    as _i8;
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart'
    as _i12;
import 'package:base_flutter/general/screens/splash/SplashImports.dart' as _i3;
import 'package:base_flutter/general/screens/terms/TermsImports.dart' as _i9;
import 'package:base_flutter/visitor/screens/vis_home/VisHomeImports.dart'
    as _i30;
import 'package:flutter/material.dart' as _i2;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<SplashRouteArgs>();
          return _i3.Splash(navigatorKey: args.navigatorKey);
        }),
    LoginRoute.name: (routeData) => _i1.CustomPage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i4.Login();
        },
        opaque: true,
        barrierDismissible: false),
    ForgetPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i5.ForgetPassword();
        }),
    ActiveAccountRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ActiveAccountRouteArgs>();
          return _i6.ActiveAccount(userId: args.userId);
        }),
    ResetPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ResetPasswordRouteArgs>();
          return _i7.ResetPassword(userId: args.userId);
        }),
    SelectLangRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i8.SelectLang();
        }),
    TermsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i9.Terms();
        }),
    AboutRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i10.About();
        }),
    ContactUsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i11.ContactUs();
        }),
    SelectUserRoute.name: (routeData) => _i1.CustomPage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i12.SelectUser();
        },
        transitionsBuilder: _i1.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 1500,
        opaque: true,
        barrierDismissible: false),
    ConfirmPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i13.ConfirmPassword();
        }),
    ChangePasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i14.ChangePassword();
        }),
    ImageZoomRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ImageZoomRouteArgs>();
          return _i15.ImageZoom(images: args.images);
        }),
    HomeRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i16.Home();
        }),
    OfferDetailsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<OfferDetailsRouteArgs>(
              orElse: () => const OfferDetailsRouteArgs());
          return _i17.OfferDetails(visitor: args.visitor);
        }),
    LanguagesRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i18.Languages();
        }),
    MyAdsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i19.MyAds();
        }),
    EditAdsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i20.EditAds();
        }),
    EditProfileRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i21.EditProfile();
        }),
    OfferTypeRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i22.OfferType();
        }),
    SwearRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<SwearRouteArgs>();
          return _i23.Swear(type: args.type);
        }),
    AddOfferDetailsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i24.AddOfferDetails();
        }),
    AddOfferPhotoRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i25.AddOfferPhoto();
        }),
    SuccessRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<SuccessRouteArgs>();
          return _i26.Success(type: args.type);
        }),
    OrderTypeRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i27.OrderType();
        }),
    AddOrderRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i28.AddOrder();
        }),
    RegisterRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i29.Register();
        }),
    VisHomeRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i30.VisHome();
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(SplashRoute.name, path: '/'),
        _i1.RouteConfig(LoginRoute.name, path: '/Login'),
        _i1.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i1.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i1.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i1.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i1.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i1.RouteConfig(AboutRoute.name, path: '/About'),
        _i1.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i1.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i1.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i1.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i1.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i1.RouteConfig(HomeRoute.name, path: '/Home'),
        _i1.RouteConfig(OfferDetailsRoute.name, path: '/offer-details'),
        _i1.RouteConfig(LanguagesRoute.name, path: '/Languages'),
        _i1.RouteConfig(MyAdsRoute.name, path: '/my-ads'),
        _i1.RouteConfig(EditAdsRoute.name, path: '/edit-ads'),
        _i1.RouteConfig(EditProfileRoute.name, path: '/edit-profile'),
        _i1.RouteConfig(OfferTypeRoute.name, path: '/offer-type'),
        _i1.RouteConfig(SwearRoute.name, path: '/Swear'),
        _i1.RouteConfig(AddOfferDetailsRoute.name, path: '/add-offer-details'),
        _i1.RouteConfig(AddOfferPhotoRoute.name, path: '/add-offer-photo'),
        _i1.RouteConfig(SuccessRoute.name, path: '/Success'),
        _i1.RouteConfig(OrderTypeRoute.name, path: '/order-type'),
        _i1.RouteConfig(AddOrderRoute.name, path: '/add-order'),
        _i1.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i1.RouteConfig(VisHomeRoute.name, path: '/vis-home')
      ];
}

class SplashRoute extends _i1.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i2.GlobalKey<_i2.NavigatorState> navigatorKey})
      : super(name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i2.GlobalKey<_i2.NavigatorState> navigatorKey;
}

class LoginRoute extends _i1.PageRouteInfo {
  const LoginRoute() : super(name, path: '/Login');

  static const String name = 'LoginRoute';
}

class ForgetPasswordRoute extends _i1.PageRouteInfo {
  const ForgetPasswordRoute() : super(name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

class ActiveAccountRoute extends _i1.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String userId})
      : super(name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(userId: userId));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.userId});

  final String userId;
}

class ResetPasswordRoute extends _i1.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String userId})
      : super(name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(userId: userId));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.userId});

  final String userId;
}

class SelectLangRoute extends _i1.PageRouteInfo {
  const SelectLangRoute() : super(name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

class TermsRoute extends _i1.PageRouteInfo {
  const TermsRoute() : super(name, path: '/Terms');

  static const String name = 'TermsRoute';
}

class AboutRoute extends _i1.PageRouteInfo {
  const AboutRoute() : super(name, path: '/About');

  static const String name = 'AboutRoute';
}

class ContactUsRoute extends _i1.PageRouteInfo {
  const ContactUsRoute() : super(name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

class SelectUserRoute extends _i1.PageRouteInfo {
  const SelectUserRoute() : super(name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

class ConfirmPasswordRoute extends _i1.PageRouteInfo {
  const ConfirmPasswordRoute() : super(name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

class ChangePasswordRoute extends _i1.PageRouteInfo {
  const ChangePasswordRoute() : super(name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

class ImageZoomRoute extends _i1.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<dynamic> images})
      : super(name,
            path: '/image-zoom', args: ImageZoomRouteArgs(images: images));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images});

  final List<dynamic> images;
}

class HomeRoute extends _i1.PageRouteInfo {
  const HomeRoute() : super(name, path: '/Home');

  static const String name = 'HomeRoute';
}

class OfferDetailsRoute extends _i1.PageRouteInfo<OfferDetailsRouteArgs> {
  OfferDetailsRoute({bool? visitor})
      : super(name,
            path: '/offer-details',
            args: OfferDetailsRouteArgs(visitor: visitor));

  static const String name = 'OfferDetailsRoute';
}

class OfferDetailsRouteArgs {
  const OfferDetailsRouteArgs({this.visitor});

  final bool? visitor;
}

class LanguagesRoute extends _i1.PageRouteInfo {
  const LanguagesRoute() : super(name, path: '/Languages');

  static const String name = 'LanguagesRoute';
}

class MyAdsRoute extends _i1.PageRouteInfo {
  const MyAdsRoute() : super(name, path: '/my-ads');

  static const String name = 'MyAdsRoute';
}

class EditAdsRoute extends _i1.PageRouteInfo {
  const EditAdsRoute() : super(name, path: '/edit-ads');

  static const String name = 'EditAdsRoute';
}

class EditProfileRoute extends _i1.PageRouteInfo {
  const EditProfileRoute() : super(name, path: '/edit-profile');

  static const String name = 'EditProfileRoute';
}

class OfferTypeRoute extends _i1.PageRouteInfo {
  const OfferTypeRoute() : super(name, path: '/offer-type');

  static const String name = 'OfferTypeRoute';
}

class SwearRoute extends _i1.PageRouteInfo<SwearRouteArgs> {
  SwearRoute({required int type})
      : super(name, path: '/Swear', args: SwearRouteArgs(type: type));

  static const String name = 'SwearRoute';
}

class SwearRouteArgs {
  const SwearRouteArgs({required this.type});

  final int type;
}

class AddOfferDetailsRoute extends _i1.PageRouteInfo {
  const AddOfferDetailsRoute() : super(name, path: '/add-offer-details');

  static const String name = 'AddOfferDetailsRoute';
}

class AddOfferPhotoRoute extends _i1.PageRouteInfo {
  const AddOfferPhotoRoute() : super(name, path: '/add-offer-photo');

  static const String name = 'AddOfferPhotoRoute';
}

class SuccessRoute extends _i1.PageRouteInfo<SuccessRouteArgs> {
  SuccessRoute({required int type})
      : super(name, path: '/Success', args: SuccessRouteArgs(type: type));

  static const String name = 'SuccessRoute';
}

class SuccessRouteArgs {
  const SuccessRouteArgs({required this.type});

  final int type;
}

class OrderTypeRoute extends _i1.PageRouteInfo {
  const OrderTypeRoute() : super(name, path: '/order-type');

  static const String name = 'OrderTypeRoute';
}

class AddOrderRoute extends _i1.PageRouteInfo {
  const AddOrderRoute() : super(name, path: '/add-order');

  static const String name = 'AddOrderRoute';
}

class RegisterRoute extends _i1.PageRouteInfo {
  const RegisterRoute() : super(name, path: '/Register');

  static const String name = 'RegisterRoute';
}

class VisHomeRoute extends _i1.PageRouteInfo {
  const VisHomeRoute() : super(name, path: '/vis-home');

  static const String name = 'VisHomeRoute';
}
