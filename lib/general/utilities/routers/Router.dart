part of 'RouterImports.dart';


@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(page: Splash, initial: true,),
    CustomRoute(page: Login,),
    AdaptiveRoute(page: ForgetPassword),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: ResetPassword),
    AdaptiveRoute(page: SelectLang),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: About),
    AdaptiveRoute(page: ContactUs),
    CustomRoute(page: SelectUser,transitionsBuilder: TransitionsBuilders.fadeIn,durationInMilliseconds: 1500),
    AdaptiveRoute(page: ConfirmPassword),
    AdaptiveRoute(page: ChangePassword),
    AdaptiveRoute(page: ImageZoom),


    //customer routes
    AdaptiveRoute(page: Home),
    AdaptiveRoute(page: OfferDetails),
    AdaptiveRoute(page: Languages),
    AdaptiveRoute(page: MyAds),
    AdaptiveRoute(page: EditAds),
    AdaptiveRoute(page: EditProfile),
    AdaptiveRoute(page: OfferType),
    AdaptiveRoute(page: Swear),
    AdaptiveRoute(page: AddOfferDetails),
    AdaptiveRoute(page: AddOfferPhoto),
    AdaptiveRoute(page: Success),
    AdaptiveRoute(page: OrderType),
    AdaptiveRoute(page: AddOrder),
    AdaptiveRoute(page: Register),


    //visitor routes
    AdaptiveRoute(page: VisHome),

  ],
)
class $AppRouter {}