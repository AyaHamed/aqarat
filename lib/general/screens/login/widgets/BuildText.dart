part of 'LoginWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MyText(
          title: tr(context,"login"),
          size: 16,
          color: MyColors.primary,
        ),
        InkWell(
          onTap: () => AutoRouter.of(context).pop(),
          child: Icon(
            Icons.arrow_forward_ios,
            color: MyColors.black,
          ),
        ),
      ],
    );
  }
}
