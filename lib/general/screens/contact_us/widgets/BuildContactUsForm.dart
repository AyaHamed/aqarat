part of 'ContactUsWidgetsImports.dart';

class BuildContactUsForm extends StatelessWidget {
  final ContactUsData contactUsData;

  const BuildContactUsForm({required this.contactUsData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: contactUsData.formKey,
      child: Column(
        children: [
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            hint: "الاسم",
            enableBorderColor: MyColors.grey,
            controller: contactUsData.name,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmail(context),
            hint: "البريد الالكتروني",
            enableBorderColor: MyColors.grey,
            controller: contactUsData.email,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            max: 5,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value!.validateEmpty(context),
            hint: "اكتب رسالتك",
            enableBorderColor: MyColors.grey,
            controller: contactUsData.msg,
            margin: const EdgeInsets.symmetric(vertical: 10),
          ),
        ],
      ),
    );
  }
}
