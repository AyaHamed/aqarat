part of 'ContactUsWidgetsImports.dart';

class BuildSocial extends StatelessWidget {
  const BuildSocial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 30),
      child: Column(
        children: [
          MyText(
            title: "أو عبر التواصل الإجتماعي",
            color: MyColors.black,
            size: 12,
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Image.asset(Res.twitter, height: 25)),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Image.asset(Res.facebook, height: 25)),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Image.asset(Res.instagram, height: 25)),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Image.asset(Res.whatsapp, height: 20)),
            ],
          )
        ],
      ),
    );
  }
}
