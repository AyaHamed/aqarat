import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';

part 'BuildContactUsForm.dart';
part 'BuildSocial.dart';