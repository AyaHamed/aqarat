part of 'ContactUsImports.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  ContactUsData contactUsData = ContactUsData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "تواصل معنا"),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        children: [
          HeaderLogo(),
          BuildContactUsForm(contactUsData: contactUsData),
          DefaultButton(
            title: "إرسال",
            onTap: () {},
            borderRadius: BorderRadius.circular(30),
          ),
          BuildSocial()
        ],
      ),
    );
  }
}
