import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart';
import 'package:base_flutter/res.dart';
import 'package:tf_validator/localization/SetLocalization.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:tf_validator/tf_validator.dart';

part 'BuildButtonList.dart';
part 'BuildVisitorDialog.dart';
part 'BuildUserText.dart';
