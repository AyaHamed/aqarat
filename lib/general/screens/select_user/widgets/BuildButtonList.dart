part of 'SelectUserWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  final SelectUserData selectUserData;
  const BuildButtonList({required this.selectUserData});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultButton(
          title: tr(context,"login"),
          onTap: () => AutoRouter.of(context).push(LoginRoute()),
          margin: const EdgeInsets.symmetric(vertical: 15),
          color: MyColors.primary,
        ),
        DefaultButton(
          title:tr(context,"register"),
          onTap: () => AutoRouter.of(context).push(RegisterRoute()),
          margin: const EdgeInsets.symmetric(horizontal: 0),
          color: MyColors.white,
          borderColor: MyColors.primary,
          textColor: MyColors.primary,
        ),
        DefaultButton(
          title: "مشاهدة عروض المكاتب",
          onTap: ()=> selectUserData.showOffers(context),
          margin: const EdgeInsets.symmetric(vertical: 15),
          color: MyColors.secondary,
          borderColor: MyColors.secondary,
        ),
      ],
    );
  }
}
