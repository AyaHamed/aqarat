part of 'SelectUserWidgetsImports.dart';

class BuildVisitorDialog extends StatelessWidget {
  const BuildVisitorDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape:
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      content: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(Res.confirmed, height: 40, width: 40),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: MyText(
                title: "جميع العروض من مكاتب وشركات عقارية معتمدة",
                color: MyColors.blackOpacity,
                size: 11,
                alien: TextAlign.center,
              ),
            ),
            DefaultButton(
              title: "موافق",
              onTap: ()=> AutoRouter.of(context).push(VisHomeRoute()),
              borderRadius: BorderRadius.circular(5),
              margin: EdgeInsets.zero,
            ),
          ],
        ),
      ),
    );
  }
}
