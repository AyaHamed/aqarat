import 'dart:ui';
import 'package:flutter/material.dart';

class MyColors{
  static Color primary =  Color(0xff1a3925);
  static Color secondary =  Color(0xff89B42A);
  static Color headerColor =  Color(0xff6e7c87);
  static Color bg=Color(0xffF8FFFD);
  static Color offWhite=Color(0xffF3F3F3);
  static Color gold=Color(0xffe4aa69);
  static Color grey=Colors.grey;
  static Color greyWhite=Colors.grey.withOpacity(.2);
  static Color black=Color(0xff031626);
  static Color blackOpacity=Colors.black54;
  static Color white=Colors.white;
  static Color notifyColor=Colors.black54;



}