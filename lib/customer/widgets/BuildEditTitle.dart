part of 'WidgetsImports.dart';

class BuildEditTitle extends StatelessWidget {
  final String title;
  const BuildEditTitle({required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 10),
      child: Row(
        children: [
          Image.asset(Res.pen, height: 15),
          SizedBox(width: 10),
          MyText(
            title: title,
            color: MyColors.primary,
            size: 10,
            fontWeight: FontWeight.w600,
          )
        ],
      ),
    );
  }
}
