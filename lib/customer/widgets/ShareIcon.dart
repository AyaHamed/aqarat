part of 'WidgetsImports.dart';

class ShareIcon extends StatelessWidget {
  const ShareIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Image.asset(
          Res.share,
          width: 25,
        ),
      ),
    );
  }
}
