part of 'WidgetsImports.dart';

class BuildItemRow extends StatelessWidget {
  final String image;
  final String text;
  final double? textSize;
  final double? imageSize;
  final EdgeInsetsGeometry? margin;
  const BuildItemRow({required this.image, required this.text, this.textSize, this.imageSize, this.margin});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Row(
        children: [
          Image.asset(image, height: imageSize ?? 12),
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              child: MyText(
                title: text,
                color: MyColors.blackOpacity,
                size: textSize ?? 9,
                fontWeight: FontWeight.w600,
              ),
            ),
          )
        ],
      ),
    );
  }
}
