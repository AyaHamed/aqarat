part of 'AddOfferDetailsImports.dart';

class AddOfferDetails extends StatefulWidget {
  const AddOfferDetails({Key? key}) : super(key: key);

  @override
  _AddOfferDetailsState createState() => _AddOfferDetailsState();
}

class _AddOfferDetailsState extends State<AddOfferDetails> {
  AddOfferDetailsData addOfferDetailsData = AddOfferDetailsData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "تفاصيل الإعلان"),
      body: Column(
        children: [
          BuildAddOfferLocation(),
          BuildAddOfferForm(addOfferDetailsData: addOfferDetailsData),
        ],
      ),
      bottomNavigationBar: DefaultButton(
        title: "إستمرار",
        onTap: ()=> AutoRouter.of(context).push(AddOfferPhotoRoute()),
      ),
    );
  }
}
