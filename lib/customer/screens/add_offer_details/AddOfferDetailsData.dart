part of 'AddOfferDetailsImports.dart';

class AddOfferDetailsData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController title = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController description = TextEditingController();
  final GenericBloc<int> phoneCubit = GenericBloc(0);
}