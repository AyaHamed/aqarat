part of 'AddOfferDetailsWidgetsImports.dart';

class BuildAddOfferForm extends StatelessWidget {
  final AddOfferDetailsData addOfferDetailsData;
  const BuildAddOfferForm({required this.addOfferDetailsData});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Form(
        key: addOfferDetailsData.formKey,
        child: GenericListView(
          type: ListViewType.normal,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          children: [

            BuildEditTitle(title: "عنوان النص"),
            GenericTextField(
              fieldTypes: FieldTypes.normal,
              type: TextInputType.text,
              action: TextInputAction.next,
              hint: "ادخل عنوان الإعلان ...",
              validate: (value) => value!.validateEmpty(context),
              controller: addOfferDetailsData.title,
              enableBorderColor: MyColors.grey.withOpacity(0.5),
            ),

            BuildChoosePhone(addOfferDetailsData: addOfferDetailsData),

            BuildEditTitle(title: "نص الإعلان"),
            GenericTextField(
              fieldTypes: FieldTypes.rich,
              type: TextInputType.text,
              max: 10,
              action: TextInputAction.done,
              hint: "التفاصيل ...",
              validate: (value) => value!.validateEmpty(context),
              controller: addOfferDetailsData.description,
              enableBorderColor: MyColors.grey.withOpacity(0.5),
            ),

            BuildAnnouncement(),
          ],
        ),
      ),
    );
  }
}
