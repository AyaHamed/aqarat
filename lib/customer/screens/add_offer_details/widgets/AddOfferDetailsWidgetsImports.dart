import 'package:base_flutter/customer/screens/add_offer_details/AddOfferDetailsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildAddOfferForm.dart';
part 'BuildChoosePhone.dart';
part 'BuilAddOfferLocation.dart';
part 'BuildAnnouncement.dart';