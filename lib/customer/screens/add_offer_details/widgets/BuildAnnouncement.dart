part of 'AddOfferDetailsWidgetsImports.dart';

class BuildAnnouncement extends StatelessWidget {
  const BuildAnnouncement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.info_outline,
            size: 20,
            color: Colors.red,
          ),
          SizedBox(width: 5),
          Expanded(
            child: MyText(
              title: "يرجى العلم أن كتابة أي وسيلة تواصل أو سعر العقار داخل نص الإعلان سوف يعرض إعلانك للحذف",
              color: Colors.red,
              size: 10,
            ),
          ),
        ],
      ),
    );
  }
}
