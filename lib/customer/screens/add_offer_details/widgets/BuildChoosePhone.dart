part of 'AddOfferDetailsWidgetsImports.dart';

class BuildChoosePhone extends StatelessWidget {
  final AddOfferDetailsData addOfferDetailsData;

  const BuildChoosePhone({required this.addOfferDetailsData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: BlocBuilder<GenericBloc<int>, GenericState<int>>(
        bloc: addOfferDetailsData.phoneCubit,
        builder: (_, state){
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 30,
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: ListTile(
                      title: MyText(
                          title: "رقم الجوال المسجل",
                          color: MyColors.primary,
                          size: 10),
                      leading: Radio(
                        value: 0,
                        groupValue: state.data,
                        onChanged: (value) =>
                            addOfferDetailsData.phoneCubit.onUpdateData(0),
                      ),
                      horizontalTitleGap: 0,
                      contentPadding: EdgeInsets.zero,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 30,
                    child: ListTile(
                      title: MyText(
                          title: "رقم جوال آخر",
                          color: MyColors.primary,
                          size: 10),
                      leading: Radio(
                        value: 1,
                        groupValue: state.data,
                        onChanged: (value) =>
                            addOfferDetailsData.phoneCubit.onUpdateData(1),
                      ),
                      horizontalTitleGap: 0,
                      contentPadding: EdgeInsets.zero,
                    ),
                  ),
                ],
              ),
              Visibility(
                visible: state.data == 1,
                child: Column(
                  children: [
                    BuildEditTitle(title: "رقم جوال آخر"),
                    GenericTextField(
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.text,
                      action: TextInputAction.next,
                      hint: "ادخل رقم جوال آخر ...",
                      validate: (value) => value!.validateEmpty(context),
                      controller: addOfferDetailsData.phone,
                      enableBorderColor: MyColors.grey.withOpacity(0.5),
                    ),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
