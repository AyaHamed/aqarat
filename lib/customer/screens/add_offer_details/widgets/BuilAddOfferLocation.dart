part of 'AddOfferDetailsWidgetsImports.dart';

class BuildAddOfferLocation extends StatelessWidget {
  const BuildAddOfferLocation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            title: "موقع السلعة",
            color: MyColors.primary,
            size: 11,
          ),
          Row(
            children: [
              Icon(
                Icons.location_on_outlined,
                size: 15,
                color: MyColors.grey,
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  child: MyText(
                    title: "الرياض",
                    color: MyColors.grey,
                    size: 11,
                  ),
                ),
              ),
              MyText(
                title: "تغيير الموقع",
                color: MyColors.primary,
                size: 9,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
