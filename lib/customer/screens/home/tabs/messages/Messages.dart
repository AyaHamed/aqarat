part of 'MessagesImports.dart';

class Messages extends StatefulWidget {
  const Messages({Key? key}) : super(key: key);

  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  MessagesData messagesData = MessagesData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "الرسائل", leading: Container()),
      body: GenericListView(
        type: ListViewType.normal,
        children: List.generate(6, (index) => BuildMessageItem()),
      ),
    );
  }
}
