part of 'MessagesWidgetsImports.dart';

class BuildMessageItem extends StatelessWidget {
  const BuildMessageItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> AutoRouter.of(context).push(OfferDetailsRoute()),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 8),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration: BoxDecoration(color: MyColors.white, boxShadow: [
          BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 5)
        ]),
        child: Column(
          children: [
            Row(
              children: [
                Icon(Icons.person_outline, size: 18),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    child: MyText(
                      title: "اسم المستخدم",
                      color: MyColors.black,
                      size: 10,
                    ),
                  ),
                ),
                MyText(title: "منذ 9 دقائق", color: MyColors.grey, size: 8),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: MyText(
                title: "نص الرسالة هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال",
                color: MyColors.blackOpacity,
                size: 9,
              ),
            ),
            Row(
              children: [
                MyText(title: "رقم الإعلان  :   ", color: MyColors.blackOpacity, size: 9),
                MyText(title: "#12345678", color: MyColors.primary, size: 9),
              ],
            )
          ],
        ),
      ),
    );
  }
}
