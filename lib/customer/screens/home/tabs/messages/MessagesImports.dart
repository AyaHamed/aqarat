import 'package:base_flutter/customer/screens/home/tabs/messages/widgets/MessagesWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'Messages.dart';
part 'MessagesData.dart';