part of 'MainPageWidgetsImports.dart';

class BuildMainPageAppBar extends PreferredSize {
  final Size preferredSize;
  BuildMainPageAppBar({
    this.preferredSize = const Size.fromHeight(kToolbarHeight + 30),
  }) : super(child: Container(), preferredSize: preferredSize);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 100,
      title: Image.asset(Res.logoHome, height: 70),
      automaticallyImplyLeading: false,
      centerTitle: true,
    );
  }
}
