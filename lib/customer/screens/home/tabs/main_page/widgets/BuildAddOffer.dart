part of 'MainPageWidgetsImports.dart';

class BuildAddOffer extends StatelessWidget {
  const BuildAddOffer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> AutoRouter.of(context).push(OfferTypeRoute()),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                color: MyColors.primary,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.add, size: 14, color: MyColors.white),
                  SizedBox(width: 5),
                  MyText(title: "إضافة إعلان", color: MyColors.white, size: 10),
                ],
              ),
            ),
            InkWell(
              onTap: ()=> AutoRouter.of(context).push(OrderTypeRoute()),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.add, size: 14, color: MyColors.white),
                    SizedBox(width: 5),
                    MyText(title: "إضافة طلب", color: MyColors.white, size: 10),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
