part of 'MainPageWidgetsImports.dart';

class BuildMainItem extends StatelessWidget {
  const BuildMainItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=> AutoRouter.of(context).push(OfferDetailsRoute(visitor: true)),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: MyColors.white,
          boxShadow: [
            BoxShadow(
              color: MyColors.greyWhite,
              spreadRadius: 2,
              blurRadius: 5,
            ),
          ],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            CachedImage(
              url:
              "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/15/a1/d2/af/hotel-r-de-paris.jpg?w=900&h=-1&s=1",
              height: 100,
              width: 120,
              borderRadius: BorderRadius.circular(5),
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: "فلل للإيجار",
                      color: MyColors.primary,
                      size: 11,
                    ),
                    BuildItemRow(
                      image: Res.map,
                      text: "24 شارع التخصصي",
                    ),
                    BuildItemRow(
                      image: Res.price,
                      text: "250 ر.س",
                    ),
                    BuildItemRow(
                      image: Res.calendar,
                      text: "22/8/2021",
                    ),
                    BuildItemRow(
                      image: Res.housedesc,
                      text: "4 غرف نوم - 3 صالة 2 دورة مياه - 800 م",
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
