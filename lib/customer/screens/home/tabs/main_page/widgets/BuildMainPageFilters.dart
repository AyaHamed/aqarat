part of 'MainPageWidgetsImports.dart';

class BuildMainPageFilters extends StatelessWidget {
  const BuildMainPageFilters({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.33,
            padding:
            const EdgeInsets.symmetric(vertical: 2, horizontal: 10),
            margin:
            const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: MyColors.grey)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(title: "المدينة", color: MyColors.grey, size: 10),
                Icon(
                  Icons.keyboard_arrow_down,
                  size: 15,
                  color: MyColors.grey,
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.33,
            padding:
            const EdgeInsets.symmetric(vertical: 2, horizontal: 10),
            margin:
            const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: MyColors.grey)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                    title: "نوع العرض", color: MyColors.grey, size: 10),
                Icon(
                  Icons.keyboard_arrow_down,
                  size: 15,
                  color: MyColors.grey,
                )
              ],
            ),
          ),
          Container(
            // height: 35, width: 35,
            padding: const EdgeInsets.all(7),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: MyColors.primary,
                boxShadow: [
                  BoxShadow(
                    color: MyColors.grey,
                    spreadRadius: 0.2,
                    blurRadius: 5,
                  )
                ]),
            alignment: Alignment.center,
            child: Icon(Icons.search, size: 20, color: MyColors.white),
          )
        ],
      ),
    );
  }
}
