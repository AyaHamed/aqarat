import 'package:base_flutter/customer/screens/home/tabs/main_page/widgets/MainPageWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'MainPage.dart';
part 'MainPageData.dart';