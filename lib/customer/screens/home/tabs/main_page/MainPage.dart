part of 'MainPageImports.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  MainPageData mainPageData = MainPageData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: BuildMainPageAppBar(),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(color: MyColors.white, boxShadow: [
              BoxShadow(
                  color: MyColors.greyWhite, spreadRadius: 3, blurRadius: 5)
            ]),
            child: Column(
              children: [
                BuildMainPageFilters(),
                BuildAddOffer(),
              ],
            ),
          ),
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              padding: const EdgeInsets.only(top: 15),
              children: List.generate(5, (index) => BuildMainItem()),
            ),
          )
        ],
      ),
    );
  }
}
