part of 'NotificationsImports.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  NotificationsData notificationsData = NotificationsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "الإشعارات", leading: Container()),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.only(top: 10),
        children: [
          BuildNotificationsItem(),
          BuildOrderNotificationItem(),
        ],
      ),
    );
  }
}
