import 'package:base_flutter/customer/screens/home/tabs/notifications/widgets/NotificationsWidgetsImports.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'Notifications.dart';
part 'NotificationsData.dart';