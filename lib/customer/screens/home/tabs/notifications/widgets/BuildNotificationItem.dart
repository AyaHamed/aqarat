part of 'NotificationsWidgetsImports.dart';

class BuildNotificationsItem extends StatelessWidget {
  const BuildNotificationsItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: MyColors.primary.withOpacity(0.3)),
          boxShadow: [
            BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 5)
          ]),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedImage(
            url:
                "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/15/a1/d2/af/hotel-r-de-paris.jpg?w=900&h=-1&s=1",
            height: 45,
            width: 45,
            haveRadius: false,
            boxShape: BoxShape.circle,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: MyText(
                title: "هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال هذا النص هو مثال",
                color: MyColors.blackOpacity,
                size: 9,
              ),
            ),
          ),
          MyText(
            title: "22 أغسطس 2021",
            color: MyColors.grey,
            size: 8,
          ),
        ],
      ),
    );
  }
}
