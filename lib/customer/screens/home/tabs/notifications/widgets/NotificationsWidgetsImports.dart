import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildNotificationItem.dart';
part 'BuildOrderNotificationItem.dart';