part of 'ProfileWidgetsImports.dart';

class BuildProfileItem extends StatelessWidget {
  final String image;
  final String title;
  final Function()? onTap;
  const BuildProfileItem({required this.image,required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.width * 0.4,
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: MyColors.primary.withOpacity(0.4)),
            boxShadow: [BoxShadow(
              color: MyColors.greyWhite, blurRadius: 5, spreadRadius: 3,
            )]
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(image, height: 50),
            SizedBox(height: 10),
            MyText(
              title: title,
              color: MyColors.primary,
              size: 12,
            ),
          ],
        ),
      ),
    );
  }
}
