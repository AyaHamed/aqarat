part of 'ProfileImports.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  ProfileData profileData = ProfileData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "حسابي", leading: Container()),
      body: Container(
        alignment: Alignment.topCenter,
        padding: const EdgeInsets.only(top: 20),
        child: Wrap(
          spacing: 20,
          runSpacing: 20,
          children: [
            BuildProfileItem(
                image: Res.profile,
                title: "الملف الشخصي",
                onTap: () => AutoRouter.of(context).push(EditProfileRoute())),
            BuildProfileItem(
                image: Res.mayadv,
                title: "إعلاناتي",
                onTap: () => AutoRouter.of(context).push(MyAdsRoute())),
            BuildProfileItem(
                image: Res.language,
                title: "لغة التطبيق",
                onTap: () => AutoRouter.of(context).push(LanguagesRoute())),
            BuildProfileItem(
                image: Res.contact,
                title: "تواصل مع الادارة",
                onTap: () => AutoRouter.of(context).push(ContactUsRoute())),
            BuildProfileItem(
                image: Res.about,
                title: "عن التطبيق",
                onTap: () => AutoRouter.of(context).push(AboutRoute())),
          ],
        ),
      ),
    );
  }
}
