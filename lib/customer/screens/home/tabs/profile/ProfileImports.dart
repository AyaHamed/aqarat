import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/edit_profile/EditProfileImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/profile/widgets/ProfileWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'Profile.dart';
part 'ProfileData.dart';