part of 'HomeWidgetsImports.dart';
class BuildTabBarPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: [
        MainPage(),
        Messages(),
        Notifications(),
        Profile(),
      ],
    );
  }
}