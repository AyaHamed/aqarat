part of 'HomeWidgetsImports.dart';

class BuildTabItem extends StatelessWidget {
  final String image;
  final String title;
  const BuildTabItem({required this.image, required this.title});

  @override
  Widget build(BuildContext context) {
    return Tab(
      text: title,
      iconMargin: EdgeInsets.symmetric(vertical: 3),
      icon: ImageIcon(
        AssetImage(image),
        size: 25,
      ),
    );
  }
}
