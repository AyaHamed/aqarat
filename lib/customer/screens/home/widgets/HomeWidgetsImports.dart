import 'package:base_flutter/customer/screens/home/tabs/main_page/MainPageImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/messages/MessagesImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/notifications/NotificationsImports.dart';
import 'package:base_flutter/customer/screens/home/tabs/profile/ProfileImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


part 'BuildTabItem.dart';
part 'BuildBottomTabBar.dart';
part 'BuildTabBarPages.dart';
