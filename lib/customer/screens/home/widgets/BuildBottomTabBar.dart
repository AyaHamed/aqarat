part of 'HomeWidgetsImports.dart';

class BuildBottomTabBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65,
      decoration: BoxDecoration(
        color: MyColors.offWhite,
        boxShadow: [
          BoxShadow(color: MyColors.greyWhite, blurRadius: 5, spreadRadius: 5)
        ],
      ),
      alignment: Alignment.topCenter,
      child: TabBar(
        unselectedLabelColor: MyColors.grey,
        unselectedLabelStyle: GoogleFonts.cairo(fontSize: 11),
        labelStyle: GoogleFonts.cairo(fontSize: 12, fontWeight: FontWeight.bold),
        labelColor: MyColors.primary,
        indicatorColor: MyColors.offWhite,
        tabs: [
          BuildTabItem(
            title: "الرئيسية",
            image: Res.homeNav,
          ),
          BuildTabItem(
            title: "الرسائل",
            image: Res.messagesNav,
          ),
          BuildTabItem(
            title: "الإشعارات",
            image: Res.notificationNav,
          ),
          BuildTabItem(
            title: "حسابي",
            image: Res.accountNav,
          ),
        ],
      ),
    );
  }
}
