part of 'AddOfferPhotoImports.dart';

class AddOfferPhoto extends StatefulWidget {
  const AddOfferPhoto({Key? key}) : super(key: key);

  @override
  _AddOfferPhotoState createState() => _AddOfferPhotoState();
}

class _AddOfferPhotoState extends State<AddOfferPhoto> {
  AddOfferPhotoData addOfferPhotoData = AddOfferPhotoData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "تفاصل الإعلان"),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          BuildAddOfferPhoto(addOfferPhotoData: addOfferPhotoData),
          BuildAddPhotoForm(addOfferPhotoData: addOfferPhotoData),
        ],
      ),
      bottomNavigationBar: DefaultButton(
        title: "إستمرار",
        onTap: ()=> AutoRouter.of(context).push(SuccessRoute(type: 1)),
      ),
    );
  }
}
