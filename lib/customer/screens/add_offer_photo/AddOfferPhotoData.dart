part of 'AddOfferPhotoImports.dart';

class AddOfferPhotoData {
  final GenericBloc<bool> checkCubit = GenericBloc(false);
  final TextEditingController price = TextEditingController();
  final TextEditingController city = TextEditingController();
}