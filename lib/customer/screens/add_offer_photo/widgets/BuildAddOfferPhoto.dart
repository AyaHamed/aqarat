part of 'AddOfferPhotoWidgetsImports.dart';

class BuildAddOfferPhoto extends StatelessWidget {
  final AddOfferPhotoData addOfferPhotoData;
  const BuildAddOfferPhoto({required this.addOfferPhotoData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          BuildEditTitle(title: "تحميل الصور"),
          Container(
            width: MediaQuery.of(context).size.width * 0.4,
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.symmetric(vertical: 30),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: MyColors.grey)),
            child: Icon(
              Icons.camera_enhance,
              color: MyColors.grey,
              size: 40,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
                bloc: addOfferPhotoData.checkCubit,
                builder: (_, state) {
                  return Checkbox(
                    value: state.data,
                    onChanged: (val) =>
                        addOfferPhotoData.checkCubit.onUpdateData(!state.data),
                    activeColor: MyColors.primary,
                  );
                },
              ),
              MyText(
                title: "عرض الصور للمشاهدين",
                color: MyColors.primary,
                size: 10,
              )
            ],
          ),
        ],
      ),
    );
  }
}
