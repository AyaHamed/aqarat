import 'package:base_flutter/customer/screens/add_offer_photo/AddOfferPhotoImports.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'BuildAddOfferPhoto.dart';
part 'BuildAddPhotoForm.dart';