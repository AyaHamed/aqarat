part of 'AddOfferPhotoWidgetsImports.dart';

class BuildAddPhotoForm extends StatelessWidget {
  final AddOfferPhotoData addOfferPhotoData;
  const BuildAddPhotoForm({required this.addOfferPhotoData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [

        BuildEditTitle(title: "أدخل السعر"),
        GenericTextField(
          fieldTypes: FieldTypes.normal,
          type: TextInputType.text,
          action: TextInputAction.next,
          hint: "ادخل السعر ...",
          validate: (value) => value!.validateEmpty(context),
          controller: addOfferPhotoData.price,
          enableBorderColor: MyColors.grey.withOpacity(0.5),
        ),

        BuildEditTitle(title: "اختر المدينة"),
        GenericTextField(
          fieldTypes: FieldTypes.normal,
          type: TextInputType.text,
          action: TextInputAction.next,
          hint: "ادخل المدينة ...",
          validate: (value) => value!.validateEmpty(context),
          controller: addOfferPhotoData.price,
          enableBorderColor: MyColors.grey.withOpacity(0.5),
        ),

        Container(
          height: 250,
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: MyColors.offWhite,
            border: Border.all(color: MyColors.secondary)
          ),
          alignment: Alignment.center,
          child: MyText(
            title: "هنا المفروض خريطة",
            color: MyColors.primary,
            size: 13,
          ),
        ),

      ],
    );
  }
}
