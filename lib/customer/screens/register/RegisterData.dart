part of 'RegisterImports.dart';

class RegisterData {
  GlobalKey<ScaffoldState> scaffold = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<CustomButtonState> btnKey = new GlobalKey<CustomButtonState>();
  // final GenericBloc<bool> showPass = GenericBloc(false);
  final GenericBloc<bool> termsCubit = GenericBloc(false);

  final TextEditingController name = new TextEditingController();
  final TextEditingController phone = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController confirmPass = new TextEditingController();
  final TextEditingController company = new TextEditingController();

  void userRegister(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    // if (formKey.currentState!.validate()) {
    //   btnKey.currentState!.animateForward();
    //
    //   btnKey.currentState!.animateReverse();
    // }
    AutoRouter.of(context).push(ActiveAccountRoute(userId: ''));
  }

}