part of 'RegisterImports.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  RegisterData registerData = RegisterData();

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          physics: BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics(),
          ),
          children: [
            HeaderLogo(),
            BuildRegisterText(),
            BuildRegisterFormInputs(registerData: registerData),
            BuildAcceptTerms(registerData: registerData),
            BuildRegisterContinueButton(registerData: registerData),
            BuildNewRegister(title: tr(context, "haveAccount")),
            BuildRegLoginButton()
          ],
        ),
      ),
    );
  }
}
