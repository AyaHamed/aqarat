part of 'RegisterWidgetsImports.dart';

class BuildRegisterContinueButton extends StatelessWidget {
  final RegisterData registerData;
  const BuildRegisterContinueButton({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return LoadingButton(
      btnKey: registerData.btnKey,
      title: tr(context,"continue"),
      onTap: () => registerData.userRegister(context),
      color: MyColors.primary,
      textColor: MyColors.white,
      margin: const EdgeInsets.symmetric(vertical: 10),
    );
  }
}
