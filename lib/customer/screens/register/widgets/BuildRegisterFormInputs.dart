part of 'RegisterWidgetsImports.dart';

class BuildRegisterFormInputs extends StatelessWidget {
  final RegisterData registerData;

  const BuildRegisterFormInputs({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "name"),
            controller: registerData.name,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "mail"),
            controller: registerData.email,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.emailAddress,
            validate: (value) => value!.validateEmail(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: tr(context, "phone"),
            controller: registerData.phone,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.phone,
            validate: (value) => value!.validatePhone(context),
            enableBorderColor: MyColors.grey,
          ),
          GenericTextField(
            fieldTypes: FieldTypes.clickable,
            label: "صورة هوية المشترك",
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
            suffixIcon: Icon(Icons.upload_rounded),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.clickable,
            label: "صورة السجل التجاري",
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
            suffixIcon: Icon(Icons.upload_rounded),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            label: "اسم المؤسسة او الشركة",
            controller: registerData.company,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey,
          ),
          // DropdownTextField<RegisterDataModel>(
          //     margin:
          //     const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          //     label: 'المدن',
          //     dropKey: data.servicesKey,
          //     useName: true,
          //     validate: (RegisterDataModel value) =>
          //         value.validateDropDown(context),
          //     onChange: data.selectService,
          //     finData: (filter) async =>
          //     await ProviderRepository(context).getServicesCats()),
          GenericTextField(
            fieldTypes: FieldTypes.password,
            label: tr(context, "password"),
            controller: registerData.password,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.next,
            type: TextInputType.text,
            validate: (value) => value!.validatePassword(context),
            enableBorderColor: MyColors.grey,
            suffixIcon: Icon(Icons.remove_red_eye, color: MyColors.grey),
          ),
          GenericTextField(
            fieldTypes: FieldTypes.password,
            label: tr(context, "confirmPass"),
            controller: registerData.password,
            margin: const EdgeInsets.symmetric(vertical: 10),
            action: TextInputAction.done,
            type: TextInputType.text,
            validate: (value) => value!.validatePasswordConfirm(context,
                pass: registerData.password.text),
            enableBorderColor: MyColors.grey,
            suffixIcon: Icon(Icons.remove_red_eye, color: MyColors.grey),
          ),
        ],
      ),
    );
  }
}
