part of 'RegisterWidgetsImports.dart';

class BuildAcceptTerms extends StatelessWidget {
  final RegisterData registerData;
  const BuildAcceptTerms({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: registerData.termsCubit,
          builder: (_, state) {
            return Checkbox(
              value: state.data,
              onChanged: (val) =>
                  registerData.termsCubit.onUpdateData(!state.data),
              activeColor: MyColors.primary,
            );
          },
        ),
        InkWell(
          onTap: () => AutoRouter.of(context).push(TermsRoute()),
          child: Row(
            children: [
              MyText(
                title: "قرأت و وافقت علي ",
                color: MyColors.black,
                size: 11,
              ),
              MyText(
                title: "الشروط والاحكام",
                color: MyColors.primary,
                decoration: TextDecoration.underline,
                size: 11,
              ),
            ],
          ),
        )
      ],
    );
  }
}
