part of 'RegisterWidgetsImports.dart';

class BuildRegisterText extends StatelessWidget {
  const BuildRegisterText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MyText(
          title: tr(context, "register"),
          size: 16,
          color: MyColors.primary,
        ),
        InkWell(
          onTap: () => AutoRouter.of(context).pop(),
          child: Icon(
            Icons.arrow_forward_ios,
            color: MyColors.black,
          ),
        ),
      ],
    );
  }
}
