part of 'OfferDetailsImports.dart';

class OfferDetailsData{
  final TextEditingController name = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController msg = TextEditingController();

  sendInfo(BuildContext context, OfferDetailsData offerDetailsData) {
    showDialog(context: context, builder: (_){
      return BuildVisitorInfoDialog(offerDetailsData: offerDetailsData);
    });
  }
  sendMessage(BuildContext context, OfferDetailsData offerDetailsData) {
    showDialog(context: context, builder: (_){
      return BuildMessageDialog(offerDetailsData: offerDetailsData);
    });
  }

}