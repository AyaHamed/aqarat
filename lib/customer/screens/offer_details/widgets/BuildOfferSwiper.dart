part of 'OfferDetailsWidgetsImports.dart';

class BuildOfferSwiper extends StatelessWidget {
  const BuildOfferSwiper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 220,
      child: Swiper(
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return CachedImage(
            url: "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/15/a1/d2/af/hotel-r-de-paris.jpg?w=900&h=-1&s=1",
            width: 100,
            height: 100,
            haveRadius: true,
          );
        },
        autoplay: true,
        scrollDirection: Axis.horizontal,
        pagination: SwiperPagination(alignment: Alignment.bottomCenter),
      ),
    );
  }
}
