part of 'OfferDetailsWidgetsImports.dart';

class BuildOfferDetailsButtons extends StatelessWidget {
  final bool visitor;
  final OfferDetailsData offerDetailsData;

  const BuildOfferDetailsButtons(
      {required this.visitor, required this.offerDetailsData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Visibility(
          visible: visitor,
          child: DefaultButton(
            title: "إرسال بياناتك",
            onTap: () => offerDetailsData.sendInfo(context, offerDetailsData),
          ),
          replacement: DefaultButton(
            title: "إرسال رسالة",
            onTap: () => offerDetailsData.sendMessage(context, offerDetailsData),
          ),
        ),
      ],
    );
  }
}
