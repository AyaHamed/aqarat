part of 'OfferDetailsWidgetsImports.dart';

class BuildOfferDetails extends StatelessWidget {
  const BuildOfferDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BuildItemRow(
            image: Res.map,
            text: "24 شارع التخصصي",
            textSize: 11,
            imageSize: 15,
            margin: const EdgeInsets.symmetric(vertical: 5),
          ),
          BuildItemRow(
            image: Res.price,
            text: "250 ر.س",
            textSize: 11,
            imageSize: 15,
            margin: const EdgeInsets.symmetric(vertical: 5),
          ),
          BuildItemRow(
            image: Res.calendar,
            text: "22/8/2021",
            textSize: 11,
            imageSize: 15,
            margin: const EdgeInsets.symmetric(vertical: 5),
          ),
          BuildItemRow(
            image: Res.housedesc,
            text: "4 غرف نوم - 3 صالة 2 دورة مياه - 800 م",
            textSize: 11,
            imageSize: 15,
            margin: const EdgeInsets.symmetric(vertical: 5),
          ),
          BuildItemRow(
            image: Res.phone,
            text: "وسيلة الاتصال",
            textSize: 11,
            imageSize: 15,
            margin: const EdgeInsets.symmetric(vertical: 3),
          ),
          MyText(
            title: "01234567890",
            color: MyColors.primary,
            size: 11,
          ),
        ],
      ),
    );
  }
}
