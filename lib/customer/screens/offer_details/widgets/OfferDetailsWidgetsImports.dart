import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/offer_details/OfferDetailsImports.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/cupertino.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';

part 'BuildOfferSwiper.dart';
part 'BuildOfferInfo.dart';
part 'BuildOfferDetails.dart';
part 'BuildOfferDetailsButtons.dart';
part 'BuildVisitorInfoDialog.dart';
part 'BuildMessageDialog.dart';