part of 'OfferDetailsWidgetsImports.dart';

class BuildVisitorInfoDialog extends StatelessWidget {
  final OfferDetailsData offerDetailsData;
  const BuildVisitorInfoDialog({required this.offerDetailsData});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      contentPadding: EdgeInsets.zero,
      content: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10),
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: "إرسال بياناتك",
                    color: MyColors.white,
                    size: 11,
                  ),
                  InkWell(
                    onTap: ()=> AutoRouter.of(context).pop(),
                    child: Icon(Icons.cancel, color: MyColors.white),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Form(
                child: Column(
                  children: [
                    Container(
                      height: 60,
                      child: GenericTextField(
                        fieldTypes: FieldTypes.normal,
                        type: TextInputType.text,
                        action: TextInputAction.next,
                        validate: (value)=> value!.validateEmpty(context),
                        hint: "اكتب اسمك هنا ...",
                        enableBorderColor: MyColors.grey,
                        controller: offerDetailsData.name,
                        margin: const EdgeInsets.symmetric(vertical: 10),
                      ),
                    ),
                    Container(
                      height: 40,
                      child: GenericTextField(
                        fieldTypes: FieldTypes.normal,
                        type: TextInputType.phone,
                        action: TextInputAction.done,
                        validate: (value)=> value!.validatePhone(context),
                        hint: "اكتب رقم جوالك هنا ...",
                        controller: offerDetailsData.phone,
                        enableBorderColor: MyColors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ),
            DefaultButton(
              title: "إرسال",
              onTap: ()=> AutoRouter.of(context).pop(),
              borderRadius: BorderRadius.circular(5),
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            ),
          ],
        ),
      ),
    );
  }
}
