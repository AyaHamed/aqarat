part of 'OfferDetailsWidgetsImports.dart';

class BuildOfferInfo extends StatelessWidget {
  const BuildOfferInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      color: MyColors.offWhite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(title: "فلل للإيجار", color: MyColors.primary, size: 13),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(title: "قبل 15 دقيقة", color: MyColors.grey, size: 9),
              MyText(title: "#1234567890", color: MyColors.grey, size: 9),
            ],
          ),
          Row(
            children: [
              Icon(
                Icons.person_outline,
                size: 20,
                color: MyColors.blackOpacity,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 5),
                child: MyText(
                  title: "اسم المستخدم",
                  color: MyColors.blackOpacity,
                  size: 10,
                ),
              ),
              Spacer(),
              Icon(
                Icons.location_on_outlined,
                size: 17,
                color: MyColors.grey,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 3),
                child: MyText(
                  title: "الرياض",
                  color: MyColors.grey,
                  size: 10,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
