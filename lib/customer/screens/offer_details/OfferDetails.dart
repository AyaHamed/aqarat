part of 'OfferDetailsImports.dart';

class OfferDetails extends StatefulWidget {
  final bool? visitor;

  const OfferDetails({this.visitor});

  @override
  _OfferDetailsState createState() => _OfferDetailsState();
}

class _OfferDetailsState extends State<OfferDetails> {
  OfferDetailsData offerDetailsData = OfferDetailsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "",
        actions: [ShareIcon()],
      ),
      body: Column(
        children: [
          BuildOfferSwiper(),
          Flexible(
            child: GenericListView(
              type: ListViewType.normal,
              children: [
                BuildOfferInfo(),
                BuildOfferDetails(),
              ],
            ),
          ),
          BuildOfferDetailsButtons(
            visitor: widget.visitor ?? false,
            offerDetailsData: offerDetailsData,
          )
        ],
      ),
    );
  }
}
