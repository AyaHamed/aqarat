part of 'AddOrderImports.dart';

class AddOrder extends StatefulWidget {
  const AddOrder({Key? key}) : super(key: key);

  @override
  _AddOrderState createState() => _AddOrderState();
}

class _AddOrderState extends State<AddOrder> {
  AddOrderData addOrderData = AddOrderData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "تفاصيل الطلب"),
      body: BuildAddOrderForm(addOrderData: addOrderData),
      bottomNavigationBar: DefaultButton(
        title: "إستمرار",
        onTap: ()=> AutoRouter.of(context).push(SuccessRoute(type: 2)),
      ),
    );
  }
}
