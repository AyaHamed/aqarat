part of 'AddOrderWidgetsImports.dart';

class BuildAddOrderForm extends StatelessWidget {
  final AddOrderData addOrderData;
  const BuildAddOrderForm({required this.addOrderData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: addOrderData.formKey,
      child: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [

          BuildEditTitle(title: "عنوان الإعلان"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            hint: "ادخل عنوان الإعلان ...",
            validate: (value) => value!.validateEmpty(context),
            controller: addOrderData.title,
            enableBorderColor: MyColors.grey.withOpacity(0.5),
          ),

          BuildOrderChoosePhone(addOrderData: addOrderData),

          BuildEditTitle(title: "نص الإعلان"),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            type: TextInputType.text,
            max: 10,
            action: TextInputAction.done,
            hint: "التفاصيل ...",
            validate: (value) => value!.validateEmpty(context),
            controller: addOrderData.description,
            enableBorderColor: MyColors.grey.withOpacity(0.5),
          ),

          BuildEditTitle(title: "اختر المدينة"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.done,
            hint: "اختر المدينة ...",
            validate: (value) => value!.validateEmpty(context),
            controller: addOrderData.city,
            enableBorderColor: MyColors.grey.withOpacity(0.5),
          ),

        ],
      ),
    );
  }
}
