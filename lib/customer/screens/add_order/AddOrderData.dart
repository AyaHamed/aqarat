part of 'AddOrderImports.dart';

class AddOrderData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController title = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController description = TextEditingController();
  final TextEditingController city = TextEditingController();
  final GenericBloc<int> phoneCubit = GenericBloc(0);
}