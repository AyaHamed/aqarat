import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/add_order/widgets/AddOrderWidgetsImports.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'AddOrder.dart';
part 'AddOrderData.dart';