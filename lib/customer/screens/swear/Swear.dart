part of 'SwearImports.dart';

class Swear extends StatefulWidget {
  final int type; //1 offer, 2 order
  const Swear({required this.type});

  @override
  _SwearState createState() => _SwearState();
}

class _SwearState extends State<Swear> {
  SwearData swearData = SwearData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "إتفاقية العمولة"),
      body: Column(
        children: [
          BuildSwearText(),
          BuildMoneySwear(swearData: swearData),
          BuildDirectSwear(swearData: swearData),
        ],
      ),
      bottomNavigationBar: BuildSwearButton(type: widget.type),
    );
  }
}
