part of 'SwearWidgetsImports.dart';

class BuildMoneySwear extends StatelessWidget {
  final SwearData swearData;

  const BuildMoneySwear({required this.swearData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 25),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: swearData.moneySwearCubit,
            builder: (_, state) {
              return Checkbox(
                value: state.data,
                onChanged: (val) =>
                    swearData.moneySwearCubit.onUpdateData(!state.data),
                activeColor: MyColors.primary,
              );
            },
          ),
          Expanded(
            child: MyText(
              title:
                  "أتعهد وأقسم بالله أنا طالب العقار أن أدفع دلالة الموقع وهي 50 ريال من دلالة الإيجار والاستثمار أو 200 ريال من دلالة البيع والله على ما أقول شهيد",
              color: MyColors.primary,
              size: 12,
            ),
          )
        ],
      ),
    );
  }
}
