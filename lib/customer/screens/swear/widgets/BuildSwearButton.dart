part of 'SwearWidgetsImports.dart';

class BuildSwearButton extends StatelessWidget {
  final int type;
  const BuildSwearButton({required this.type});

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: type == 1,
      child: DefaultButton(
        title: "إستمرار",
        onTap: ()=> AutoRouter.of(context).push(AddOfferDetailsRoute()),
      ),
      replacement: DefaultButton(
        title: "إستمرار",
        onTap: ()=> AutoRouter.of(context).push(AddOrderRoute()),
      ),
    );
  }
}
