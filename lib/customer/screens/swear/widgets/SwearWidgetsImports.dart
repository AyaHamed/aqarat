import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/swear/SwearImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildSwearText.dart';
part 'BuildMoneySwear.dart';
part 'BuildDirectSwear.dart';
part 'BuildSwearButton.dart';