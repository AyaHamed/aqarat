part of 'SwearWidgetsImports.dart';

class BuildSwearText extends StatelessWidget {
  const BuildSwearText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      color: MyColors.offWhite,
      alignment: Alignment.center,
      child: Column(
        children: [
          MyText(
            title: "بسم الله الرحمن الرحيم",
            color: MyColors.primary,
            size: 12,
            alien: TextAlign.center,
          ),
          SizedBox(height: 10),
          MyText(
            title: "وَأَوْفُوا بِعَهْدِ اللَّهِ إِذَا عَاهَدتُّمْ وَلَا تَنقُضُوا الْأَيْمَانَ بَعْدَ تَوْكِيدِهَا وَقَدْ جَعَلْتُمُ اللَّهَ عَلَيْكُمْ كَفِيلًا ۚ",
            color: MyColors.primary,
            size: 12,
            alien: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
