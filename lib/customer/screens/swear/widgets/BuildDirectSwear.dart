part of 'SwearWidgetsImports.dart';

class BuildDirectSwear extends StatelessWidget {
  final SwearData swearData;
  const BuildDirectSwear({required this.swearData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
            bloc: swearData.directSwearCubit,
            builder: (_, state) {
              return Checkbox(
                value: state.data,
                onChanged: (val) =>
                    swearData.directSwearCubit.onUpdateData(!state.data),
                activeColor: MyColors.primary,
              );
            },
          ),
          Expanded(
            child: MyText(
              title: "هل أنت مباشر مع صاحب الطلب أو مع الوكيل وتتحمل كامل المسؤولية",
              color: MyColors.primary,
              size: 12,
            ),
          )
        ],
      ),
    );
  }
}
