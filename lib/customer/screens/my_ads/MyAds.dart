part of 'MyAdsImports.dart';

class MyAds extends StatefulWidget {
  const MyAds({Key? key}) : super(key: key);

  @override
  _MyAdsState createState() => _MyAdsState();
}

class _MyAdsState extends State<MyAds> {
  MyAdsData myAdsData = MyAdsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "إعلاناتي"),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.only(top: 10),
        children: List.generate(
          4,
          (index) => BuildMyAdsItem(),
        ),
      ),
    );
  }
}
