import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'BuildMyAdsItem.dart';