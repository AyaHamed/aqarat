part of 'MyAdsWidgetsImports.dart';

class BuildMyAdsItem extends StatelessWidget {
  const BuildMyAdsItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(
            color: MyColors.greyWhite,
            spreadRadius: 3,
            blurRadius: 3,
          ),
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            children: [
              CachedImage(
                url:
                    "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/15/a1/d2/af/hotel-r-de-paris.jpg?w=900&h=-1&s=1",
                height: 90,
                width: 110,
                borderRadius: BorderRadius.circular(5),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: "فلل للإيجار",
                        color: MyColors.primary,
                        size: 11,
                      ),
                      BuildItemRow(
                        image: Res.map,
                        text: "24 شارع التخصصي",
                      ),
                      BuildItemRow(
                        image: Res.price,
                        text: "250 ر.س",
                      ),
                      BuildItemRow(
                        image: Res.calendar,
                        text: "22/8/2021",
                      ),
                      BuildItemRow(
                        image: Res.housedesc,
                        text: "4 غرف نوم - 3 صالة 2 دورة مياه - 800 م",
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Divider(thickness: 1),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: ()=> AutoRouter.of(context).push(EditAdsRoute()),
                child: Container(
                  height: 30,
                  width: MediaQuery.of(context).size.width*0.35,
                  decoration: BoxDecoration(
                    color: MyColors.primary,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.edit, color: MyColors.white, size: 15,),
                      SizedBox(width: 5),
                      MyText(title: "تعديل",color: MyColors.white, size: 9,)
                    ],
                  ),
                ),
              ),
              InkWell(
                child: Container(
                  height: 30,
                  width: MediaQuery.of(context).size.width*0.35,
                  decoration: BoxDecoration(
                    color: Colors.red[700],
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.delete_outline, color: MyColors.white, size: 15,),
                      SizedBox(width: 5),
                      MyText(title: "حذف",color: MyColors.white, size: 9,)
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
