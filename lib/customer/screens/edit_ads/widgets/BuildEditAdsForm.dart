part of 'EditAdsWidgetsImports.dart';

class BuildEditAdsForm extends StatelessWidget {
  final EditAdsData editAdsData;

  const BuildEditAdsForm({required this.editAdsData});

  @override
  Widget build(BuildContext context) {
    return Form(
      child: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          BuildEditTitle(title: "عنوان الإعلان"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            hint: "ادخل عنوان الإعلان ...",
            validate: (value) => value!.validateEmpty(context),
            controller: editAdsData.title,
            enableBorderColor: MyColors.grey.withOpacity(0.5),
          ),
          BuildEditTitle(title: "نص الإعلان"),
          GenericTextField(
            fieldTypes: FieldTypes.rich,
            max: 5,
            hint: "التفاصيل ...",
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            controller: editAdsData.desc,
            enableBorderColor: MyColors.grey.withOpacity(0.5),
          ),
          BuildEditTitle(title: "صور العقار"),
          GenericTextField(
            fieldTypes: FieldTypes.clickable,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey.withOpacity(0.5),
            prefixIcon: Icon(Icons.upload_rounded),
          ),
          BuildEditTitle(title: "أدخل السعر"),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.number,
            hint: "أدخل السعر ...",
            action: TextInputAction.next,
            validate: (value) => value!.validateEmpty(context),
            enableBorderColor: MyColors.grey.withOpacity(0.5),
            controller: editAdsData.price,
          ),
          BuildEditTitle(title: "اختر المدينة"),
          // DropdownTextField<RegisterDataModel>(
          //     margin:
          //     const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
          //     label: 'اختر',
          //     dropKey: data.servicesKey,
          //     useName: true,
          //     validate: (RegisterDataModel value) =>
          //         value.validateDropDown(context),
          //     onChange: data.selectService,
          //     finData: (filter) async =>
          //     await ProviderRepository(context).getServicesCats()),
        ],
      ),
    );
  }
}
