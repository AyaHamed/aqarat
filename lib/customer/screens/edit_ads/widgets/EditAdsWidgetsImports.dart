import 'package:base_flutter/customer/screens/edit_ads/EditAdsImports.dart';
import 'package:base_flutter/customer/widgets/WidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildEditAdsForm.dart';