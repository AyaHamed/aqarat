part of 'EditAdsImports.dart';

class EditAds extends StatefulWidget {
  const EditAds({Key? key}) : super(key: key);

  @override
  _EditAdsState createState() => _EditAdsState();
}

class _EditAdsState extends State<EditAds> {
  EditAdsData editAdsData = EditAdsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "تعديل الإعلان"),
      body: BuildEditAdsForm(editAdsData: editAdsData),
      bottomNavigationBar: DefaultButton(
        title: "حفظ التعديلات",
        onTap: () {},
      ),
    );
  }
}
