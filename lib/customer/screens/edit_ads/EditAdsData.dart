part of 'EditAdsImports.dart';

class EditAdsData{
  final TextEditingController title = TextEditingController();
  final TextEditingController desc = TextEditingController();
  final TextEditingController price = TextEditingController();
  final TextEditingController city = TextEditingController();
}