part of 'OfferTypeImports.dart';

class OfferType extends StatefulWidget {
  const OfferType({Key? key}) : super(key: key);

  @override
  _OfferTypeState createState() => _OfferTypeState();
}

class _OfferTypeState extends State<OfferType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "إضافة إعلان"),
      body: Container(
        margin: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            BuildOfferTypeItem(
              title: "عرض عقار للبيع",
              image: Res.sell,
              onTap: ()=> AutoRouter.of(context).push(SwearRoute(type: 1)),
            ),
            BuildOfferTypeItem(
              title: "عرض عقار للإيجار",
              image: Res.rent,
              onTap: ()=> AutoRouter.of(context).push(SwearRoute(type: 1)),
            ),
          ],
        ),
      ),
    );
  }
}
