part of 'OfferTypeWidgetsImports.dart';

class BuildOfferTypeItem extends StatelessWidget {
  final String title;
  final String image;
  final Function()? onTap;
  const BuildOfferTypeItem({required this.title, required this.image, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Column(
          children: [
            Row(
              children: [
                Image.asset(image, height: 25),
                SizedBox(width: 15),
                MyText(
                  title: title,
                  color: MyColors.primary,
                  size: 13,
                  fontWeight: FontWeight.w600,
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 10),
              child: Divider(thickness: 1),
            ),
          ],
        ),
      ),
    );
  }
}
