import 'package:base_flutter/customer/screens/languages/widgets/LanguagesWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';

part 'Languages.dart';
part 'LanguagesData.dart';
