part of 'LanguagesImports.dart';

class Languages extends StatefulWidget {
  @override
  _LanguagesState createState() => _LanguagesState();
}

class _LanguagesState extends State<Languages> {
  final LanguagesData languagesData = new LanguagesData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: DefaultButton(
        title: 'حفظ التعديلات',
        onTap: (){},
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      ),
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: "اللغة",
      ),
      body: BuildLanguagesView(
        languagesData: languagesData,
      ),
    );
  }
}
