import 'package:base_flutter/customer/screens/languages/LanguagesImports.dart';
import 'package:base_flutter/general/blocks/lang_cubit/lang_cubit.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'BuildLanguagesView.dart';
