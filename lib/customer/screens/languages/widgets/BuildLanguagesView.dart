part of 'LanguagesWidgetsImports.dart';

class BuildLanguagesView extends StatelessWidget {
  final LanguagesData languagesData;
  const BuildLanguagesView({required this.languagesData});

  @override
  Widget build(BuildContext context) {
    var lang = context.watch<LangCubit>().state.locale;
    return ListView(
      physics: AlwaysScrollableScrollPhysics(
        parent: BouncingScrollPhysics(),
      ),
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: MyText(
                  title: "العربية",
                  size: 12,
                  color: MyColors.blackOpacity,
                ),
              ),
            ),
            Radio(
              value: "ar",
              groupValue: lang,
              onChanged: (val){},
            ),
          ],
        ),
        Divider(
          thickness: 1,
          height: 15,
          color: MyColors.greyWhite,
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: MyText(
                  title: "English",
                  size: 12,
                  color: MyColors.blackOpacity,
                ),
              ),
            ),
            Radio(
              value: "en",
              groupValue: lang,
              onChanged: (val){},
            ),
          ],
        ),
        Divider(
          thickness: 1,
          height: 20,
          color: MyColors.greyWhite,
        ),
      ],
    );
  }
}
