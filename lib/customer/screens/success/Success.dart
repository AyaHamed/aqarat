part of 'SuccessImports.dart';

class Success extends StatefulWidget {
  final int type; //1 offer , 2 order
  const Success({required this.type});

  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends State<Success> {
  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 2000), ((){
      if(widget.type ==1){
        AutoRouter.of(context).push(HomeRoute());
      } else {
        AutoRouter.of(context).push(AddOrderRoute());
      }
    }));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.primary,
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            Res.success,
          ),
          SizedBox(height: 20),
          Visibility(
            visible: widget.type == 1,
            child: MyText(
              title: "تم إضافة إعلانك بنجاح",
              color: MyColors.white,
              size: 18,
              alien: TextAlign.center,
            ),
            replacement: MyText(
              title: "تم إرسال طلبك بنجاح",
              color: MyColors.white,
              size: 18,
              alien: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
