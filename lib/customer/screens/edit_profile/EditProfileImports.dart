import 'package:base_flutter/customer/screens/edit_profile/widgets/EditProfileWidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/widgets/GenericListView.dart';

part 'EditProfile.dart';
part 'EditProfileData.dart';