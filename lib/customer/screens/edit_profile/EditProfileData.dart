part of 'EditProfileImports.dart';

class EditProfileData{
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController company = TextEditingController();
  final TextEditingController city = TextEditingController();
}