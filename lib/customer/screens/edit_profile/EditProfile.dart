part of 'EditProfileImports.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  EditProfileData editProfileData = EditProfileData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(
        title: "الملف الشخصي",
        actions: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: Image.asset(
              Res.logout,
              width: 25,
              color: MyColors.white,
            ),
          ),
        ],
      ),
      body: GenericListView(
        type: ListViewType.normal,
        padding: const EdgeInsets.only(top: 20, right: 15, left: 15),
        children: [
          BuildProfilePhoto(),
          BuildProfileForm(editProfileData: editProfileData)
        ],
      ),
      bottomNavigationBar: BuildProfileButtons(),
    );
  }
}
