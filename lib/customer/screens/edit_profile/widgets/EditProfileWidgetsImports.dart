import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/edit_profile/EditProfileImports.dart';
import 'package:tf_validator/validator/Validator.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'BuildProfileButtons.dart';
part 'BuildProfilePhoto.dart';
part 'BuildProfileForm.dart';