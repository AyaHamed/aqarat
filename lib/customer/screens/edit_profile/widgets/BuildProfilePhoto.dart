part of 'EditProfileWidgetsImports.dart';

class BuildProfilePhoto extends StatelessWidget {
  const BuildProfilePhoto({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Container(
              height: 150,
              width: 150,
              margin: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                color: MyColors.grey.withOpacity(0.7),
                shape: BoxShape.circle,
                border: Border.all(color: MyColors.grey),
                image: DecorationImage(
                  image: NetworkImage(
                      "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/15/a1/d2/af/hotel-r-de-paris.jpg?w=900&h=-1&s=1"
                  ),
                  fit: BoxFit.fill
                )
              ),
            ),
            Positioned(
              right: 10,
              child: Container(
                height: 35, width: 35,
                decoration: BoxDecoration(
                  color: MyColors.black,
                  shape: BoxShape.circle,
                ),
                alignment: Alignment.center,
                child: Icon(Icons.edit, color: MyColors.white, size: 20),
              ),
            )
          ],
        ),
      ],
    );
  }
}
