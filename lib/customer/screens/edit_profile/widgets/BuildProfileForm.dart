part of 'EditProfileWidgetsImports.dart';

class BuildProfileForm extends StatelessWidget {
  final EditProfileData editProfileData;

  const BuildProfileForm({required this.editProfileData});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: editProfileData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          MyText(title: "الاسم", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmpty(context),
            hint: "الاسم",
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: editProfileData.name,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "البريد الالكتروني", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmail(context),
            hint: "البريد الالكتروني",
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: editProfileData.email,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "رقم الجوال", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.phone,
            action: TextInputAction.done,
            validate: (value)=> value!.validatePhone(context),
            hint: "رقم الجوال",
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: editProfileData.phone,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "اسم المؤسسة او الشركة", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmpty(context),
            hint: "اسم المؤسسة او الشركة",
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: editProfileData.company,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "المدن", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmpty(context),
            hint: "المدن",
            suffixIcon: Icon(Icons.edit),
            enableBorderColor: MyColors.grey,
            controller: editProfileData.city,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "صورة الهوية", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.clickable,
            type: TextInputType.text,
            action: TextInputAction.next,
            validate: (value)=> value!.validateEmpty(context),
            hint: "file.jpg",
            enableBorderColor: MyColors.grey,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),

          MyText(title: "صورة السجل التجاري", color: MyColors.blackOpacity, size: 11, fontWeight: FontWeight.w600,),
          GenericTextField(
            fieldTypes: FieldTypes.normal,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value)=> value!.validateEmpty(context),
            hint: "file.jpg",
            enableBorderColor: MyColors.grey,
            margin: const EdgeInsets.only(top: 5, bottom: 15),
          ),
        ],
      ),
    );
  }
}
