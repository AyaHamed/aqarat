part of 'EditProfileWidgetsImports.dart';

class BuildProfileButtons extends StatelessWidget {
  const BuildProfileButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      child: Column(
        children: [
          DefaultButton(
            title: "تغيير كلمة المرور",
            onTap: ()=> AutoRouter.of(context).push(ChangePasswordRoute()),
            color: MyColors.white,
            borderColor: MyColors.primary,
            textColor: MyColors.primary,
            margin: const EdgeInsets.symmetric(horizontal: 20),
          ),
          DefaultButton(
            title: "حفظ التعديلات",
            onTap: () {},
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          ),
        ],
      ),
    );
  }
}
