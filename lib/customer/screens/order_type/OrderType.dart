part of 'OrderTypeImports.dart';

class OrderType extends StatefulWidget {
  const OrderType({Key? key}) : super(key: key);

  @override
  _OrderTypeState createState() => _OrderTypeState();
}

class _OrderTypeState extends State<OrderType> {
  OrderTypeData orderTypeData = OrderTypeData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.bg,
      appBar: DefaultAppBar(title: "إضافة طلب جديد"),
      body: Container(
        margin: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            BuildOfferTypeItem(
              title: "طلب عقار للبيع",
              image: Res.sell,
              onTap: ()=> AutoRouter.of(context).push(SwearRoute(type: 2)),
            ),
            BuildOfferTypeItem(
              title: "طلب عقار للإيجار",
              image: Res.rent,
              onTap: ()=> AutoRouter.of(context).push(SwearRoute(type: 2)),
            ),
          ],
        ),
      ),
    );
  }
}
